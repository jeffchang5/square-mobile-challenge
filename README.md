## Square's Mobile Project

1. Build Tools & Versions Used.**

   Android Studio 3.6.2 - should require nothing more out of the box

2. **Your Focus Areas.** 

   Software Architecture, Code Style, Readability, Robustness and other standard Android development practices.

   * Rotation works
   * Malformed JSON 
   * Limited internet connectivity

3. **Copied-in code or copied-in dependencies.** 

   I was working on a presentational app over the weekend that uses an API for COVID-19 data. I was practicing some stuff with coroutines but also copied some utilities that I've used in the past.

4. **Tablet / phone focus.** 

   Both works fine. 

5. **How long you spent on the project.** 

   Around 5 hours. Most of the work was templated out but testing, accounting for rotation, and UI work took me the bulk of the time.

6. **Anything else you want us to know.**

   If I had more time

   * I would do more extensive testing with Internet connectivity. 
   * And more tests for different architectural layers of this app.
   * The Glide implementation can be optimized with its integration with RecyclerView.

### Screenshots
<table>
    <tr>
        <td><img style="width: 350px: height:auto; margin: 0 50px" src="/screenshots/pixel3-horizontal.png"></img></td>
        <td><img src="screenshots/pixel3-vertical.png"></img></td>
        <td><img src="screenshots/pixelc.png"></img></td>
    </tr>
        <tr>
        <td align="center"><b>Pixel 3 API 29</b></img></td>
        <td align="center"><b>Pixel 3 API 29</b></img></td>
        <td align="center"><b>Pixel C API 29<b></td>
    </tr>
</table>
