package io.jeffchang

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.jeffchang.template.core.data.Failure
import io.jeffchang.template.core.data.HostNotFoundNetworkException
import io.jeffchang.template.core.data.Success
import io.jeffchang.template.core.data.TestContextProvider
import io.jeffchang.template.core.data.model.ViewState
import io.jeffchang.template.data.model.Employee
import io.jeffchang.template.data.model.EmployeeType
import io.jeffchang.template.domain.interaction.weather.GetEmployeesUseCase
import io.jeffchang.template.ui.employee.viewmodel.EmployeeViewModel
import io.jeffchang.util.connectivity.TestConnectivity
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class EmployeeViewModelTest {

    private var getEmployees: GetEmployeesUseCase = mock()
    private val coroutineContext = TestContextProvider()
    private val testConnectivity = TestConnectivity()
    private val employeeViewModel by lazy {
        EmployeeViewModel(coroutineContext, testConnectivity, getEmployees)
    }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Test
    fun `non-empty employee list is successful`() {
        runBlocking {
            whenever(getEmployees(Unit))
                .thenReturn(
                    Success(
                        listOf(
                            Employee(
                                EmployeeType.FULL_TIME,
                                "10",
                                "10",
                                "",
                                "",
                                "",
                                "",
                                "",
                                ""
                            )
                        )
                    )
                )
            employeeViewModel.getEmployees()

            val success = (employeeViewModel.viewState().value as ViewState.Success)
            employeeViewModel.viewState().value shouldBeInstanceOf ViewState.Success::class
        }
    }
    @Test
    fun `empty employee list is empty`() {
        runBlocking {
            whenever(getEmployees(Unit))
                .thenReturn(
                    Success(listOf())
                )
            employeeViewModel.getEmployees()

            employeeViewModel.viewState().value shouldBeInstanceOf ViewState.Empty::class
        }
    }

    @Test
    fun `no internet getting employees leads to an error`() {
        runBlocking {
            whenever(getEmployees(Unit))
                .thenReturn(Failure(HostNotFoundNetworkException))
            employeeViewModel.viewState().value shouldBeInstanceOf ViewState.Error::class
        }
    }
}
