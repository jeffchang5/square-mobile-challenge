package io.jeffchang.util.connectivity

import io.jeffchang.template.core.data.model.NetworkState
import io.jeffchang.template.core.network.Connectivity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class TestConnectivity : Connectivity {

    override val networkState: Flow<NetworkState>
        get() = flow {}
}
