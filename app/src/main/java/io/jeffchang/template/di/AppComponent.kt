package io.jeffchang.template.di

import dagger.Component
import io.jeffchang.template.EmployeeApplication
import io.jeffchang.template.core.CoreComponent
import io.jeffchang.template.di.scope.AppScope

@AppScope
@Component(dependencies = [CoreComponent::class])
interface AppComponent {

    fun inject(application: EmployeeApplication)

    @Component.Builder
    interface Builder {

        fun coreComponent(coreComponent: CoreComponent): Builder

        fun build(): AppComponent
    }
}
