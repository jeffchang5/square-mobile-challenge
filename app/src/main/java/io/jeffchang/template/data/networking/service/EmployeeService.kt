package io.jeffchang.template.data.networking.service

import io.jeffchang.template.data.model.EmployeeResponse
import retrofit2.http.GET

interface EmployeeService {

    @GET("employees.json")
    suspend fun getEmployees(): EmployeeResponse
}
