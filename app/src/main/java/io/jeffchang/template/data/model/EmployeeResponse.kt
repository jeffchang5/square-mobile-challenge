package io.jeffchang.template.data.model

import com.squareup.moshi.Json
import io.jeffchang.template.core.data.DomainMapper

data class EmployeeResponse(

    @Json(name = "employees")
    val employees: List<Employee>

) : DomainMapper<List<Employee>> {

    override fun mapToDomainModel(): List<Employee> = employees
}
