package io.jeffchang.template.data.model

import com.squareup.moshi.Json

enum class EmployeeType(val title: String) {
    @Json(name = "FULL_TIME")
    FULL_TIME("Full Time"),

    @Json(name = "PART_TIME")
    PART_TIME("Part Time"),

    @Json(name = "CONTRACTOR")
    CONTRACTOR("Contractor")
}

data class Employee(

    @Json(name = "employee_type")
    val employeeEmployeeType: EmployeeType,

    @Json(name = "full_name")
    val fullName: String,

    @Json(name = "email_address")
    val emailAddress: String,

    @Json(name = "photo_url_large")
    val photoUrlLarge: String? = null,

    @Json(name = "photo_url_small")
    val photoUrlSmall: String? = null,

    @Json(name = "phone_number")
    val phoneNumber: String? = null,

    @Json(name = "biography")
    val biography: String? = null,

    @Json(name = "team")
    val team: String,

    @Json(name = "uuid")
    val uuid: String
)
