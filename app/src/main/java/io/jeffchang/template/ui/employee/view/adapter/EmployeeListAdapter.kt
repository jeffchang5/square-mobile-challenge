package io.jeffchang.template.ui.employee.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.jeffchang.template.R
import io.jeffchang.template.core.databinding.ItemEmployeeBinding
import io.jeffchang.template.core.ktx.onClick
import io.jeffchang.template.data.model.Employee

class EmployeeListAdapter(
    private val onEmployeeClicked: (Employee) -> Unit = {}
) : ListAdapter<Employee, EmployeeListAdapter.EmployeeViewHolder>(EmployeeDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val binding = ItemEmployeeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return EmployeeViewHolder(binding).also {
            it.itemView.onClick {
                onEmployeeClicked(getItem(it.adapterPosition))
            }
        }
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class EmployeeViewHolder(private val binding: ItemEmployeeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(employee: Employee) {
            binding.apply {
                employeeTextView.text = employee.fullName
                teamTextView.text = employee.team

                Glide.with(root.context)
                    .load(employee.photoUrlSmall)
                    .apply(
                        RequestOptions()
                            .circleCrop()
                            .error(R.drawable.ic_bad_mood)
                    )
                    .into(employeeImageView)
            }
        }
    }

    private class EmployeeDiffCallback : DiffUtil.ItemCallback<Employee>() {
        override fun areItemsTheSame(oldItem: Employee, newItem: Employee): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: Employee, newItem: Employee): Boolean =
            oldItem == newItem
    }
}
