package io.jeffchang.template.ui.employee.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import io.jeffchang.template.core.base.BaseFragment
import io.jeffchang.template.core.base.BaseViewModel
import io.jeffchang.template.core.base.State
import io.jeffchang.template.core.data.model.NetworkState
import io.jeffchang.template.core.data.model.ViewState
import io.jeffchang.template.core.ktx.invoke
import io.jeffchang.template.data.model.Employee
import io.jeffchang.template.databinding.FragmentEmployeeBinding
import io.jeffchang.template.ui.employee.di.inject
import io.jeffchang.template.ui.employee.view.adapter.EmployeeListAdapter
import io.jeffchang.template.ui.employee.viewmodel.EmployeeViewModel
import timber.log.Timber
import javax.inject.Inject

class EmployeeFragment : BaseFragment<List<Employee>>() {

    private lateinit var binding: FragmentEmployeeBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val employeeViewModel by viewModels<EmployeeViewModel> {
        viewModelFactory
    }

    override val viewModel: BaseViewModel<List<Employee>>
        get() = employeeViewModel

    override lateinit var containerView: View

    private val employeeAdapter by lazy {
        EmployeeListAdapter(::navigate)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEmployeeBinding.inflate(inflater, container, false).also {
            containerView = it.root
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        initSwipeRefreshLayout()

        subscribeUI()
    }

    private fun subscribeUI() {
        binding.apply {
            employeeViewModel.networkState()(viewLifecycleOwner) {
                state = when (it) {
                    is NetworkState.Connected -> {
                        State.MAIN
                    }
                    is NetworkState.Disconnected -> {
                        State.NO_INTERNET
                    }
                }
            }
            employeeViewModel.viewState()(viewLifecycleOwner) {
                when (it) {
                    is ViewState.Success -> {
                        binding.swipeRefreshLayout.isRefreshing = false
                        employeeAdapter.submitList(it.data)
                        state = State.MAIN
                    }
                    is ViewState.Error -> {
                        state = State.ERROR
                    }
                    is ViewState.Empty -> {
                        state = State.EMPTY
                    }
                }
            }
        }
    }

    private fun initRecyclerView() {
        binding.employeeRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
            )
            adapter = employeeAdapter
        }
    }

    private fun initSwipeRefreshLayout() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            employeeViewModel.getEmployees()
        }
    }

    private fun navigate(employee: Employee) {
        // TODO: Navigate
        Timber.d(employee.toString())
    }
}
