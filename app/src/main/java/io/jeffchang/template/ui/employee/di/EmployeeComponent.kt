package io.jeffchang.template.ui.employee.di

import dagger.Component
import io.jeffchang.template.core.CoreComponent
import io.jeffchang.template.core.di.BaseFragmentComponent
import io.jeffchang.template.core.module.ViewModelModule
import io.jeffchang.template.core.scope.FeatureScope
import io.jeffchang.template.ui.employee.di.module.EmployeeDataModule
import io.jeffchang.template.ui.employee.di.module.EmployeeModule
import io.jeffchang.template.ui.employee.view.EmployeeFragment

/**
 * Component binding injections for employee related classes
 */
@Component(
    modules = [ViewModelModule::class, EmployeeModule::class, EmployeeDataModule::class],
    dependencies = [CoreComponent::class]
)
@FeatureScope
interface EmployeeComponent : BaseFragmentComponent<EmployeeFragment> {

    @Component.Builder
    interface Builder {

        fun coreComponent(component: CoreComponent): Builder

        fun build(): EmployeeComponent
    }
}
