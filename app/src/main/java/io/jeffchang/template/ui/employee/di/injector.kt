package io.jeffchang.template.ui.employee.di

import io.jeffchang.template.coreComponent
import io.jeffchang.template.ui.employee.view.EmployeeFragment

fun EmployeeFragment.inject() {
    DaggerEmployeeComponent.builder()
        .coreComponent(coreComponent())
        .build()
        .inject(this)
}
