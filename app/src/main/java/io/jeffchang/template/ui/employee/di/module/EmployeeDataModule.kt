package io.jeffchang.template.ui.employee.di.module

import dagger.Module
import dagger.Provides
import io.jeffchang.template.core.data.ContextProvider
import io.jeffchang.template.core.scope.FeatureScope
import io.jeffchang.template.data.networking.service.EmployeeService
import io.jeffchang.template.domain.interaction.weather.DefaultGetEmployeesUseCase
import io.jeffchang.template.domain.interaction.weather.GetEmployeesUseCase
import io.jeffchang.template.domain.repository.DefaultEmployeeRepository
import io.jeffchang.template.domain.repository.EmployeeRepository
import retrofit2.Retrofit

@Module
class EmployeeDataModule {

    @Provides
    @FeatureScope
    fun provideEmployeeService(retrofit: Retrofit): EmployeeService =
        retrofit.create(EmployeeService::class.java)

    @Provides
    @FeatureScope
    fun provideEmployeeRepository(
        contextProvider: ContextProvider,
        employeeService: EmployeeService
    ): EmployeeRepository =
        DefaultEmployeeRepository(contextProvider, employeeService)

    @Provides
    @FeatureScope
    fun provideGetEmployeeUseCase(
        employeeRepository: EmployeeRepository
    ): GetEmployeesUseCase = DefaultGetEmployeesUseCase(employeeRepository)
}
