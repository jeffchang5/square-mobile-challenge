package io.jeffchang.template.ui.employee.viewmodel

import io.jeffchang.template.core.base.BaseViewModel
import io.jeffchang.template.core.data.ContextProvider
import io.jeffchang.template.core.data.HostNotFoundNetworkException
import io.jeffchang.template.core.data.model.NetworkState
import io.jeffchang.template.core.data.model.ViewState
import io.jeffchang.template.core.data.onFailure
import io.jeffchang.template.core.data.onSuccess
import io.jeffchang.template.core.network.Connectivity
import io.jeffchang.template.data.model.Employee
import io.jeffchang.template.domain.interaction.weather.GetEmployeesUseCase
import javax.inject.Inject
import timber.log.Timber

class EmployeeViewModel @Inject constructor(
    contextProvider: ContextProvider,
    connectivity: Connectivity,
    private val getEmployeesUseCase: GetEmployeesUseCase
) : BaseViewModel<List<Employee>>(contextProvider, connectivity) {

    override var onConnectivityRegained: () -> Unit = ::getEmployees

    init {
        getEmployees()
    }

    fun getEmployees() {
        execute {
            getEmployeesUseCase(Unit)
                .onSuccess {
                    if (it.isEmpty()) {
                        viewState.value = ViewState.Empty()
                        return@onSuccess
                    }
                    Timber.d("Received employees")
                    viewState.value = ViewState.Success(it)
                }
                .onFailure {
                    if (it is HostNotFoundNetworkException) {
                        Timber.d("Error receiving employees because of internet")
                        networkState.postValue(NetworkState.Disconnected)
                    }
                    Timber.d("Error receiving employees")
                    viewState.value = ViewState.Error(it)
                }
        }
    }
}
