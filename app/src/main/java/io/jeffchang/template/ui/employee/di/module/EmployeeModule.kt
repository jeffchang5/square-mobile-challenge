package io.jeffchang.template.ui.employee.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.jeffchang.template.core.di.ViewModelKey
import io.jeffchang.template.ui.employee.viewmodel.EmployeeViewModel

@Module
abstract class EmployeeModule {

    @Binds
    @IntoMap
    @ViewModelKey(EmployeeViewModel::class)
    abstract fun bindEmployeeViewModel(viewModel: EmployeeViewModel): ViewModel
}
