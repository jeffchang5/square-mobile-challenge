package io.jeffchang.template

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import io.jeffchang.template.core.CoreComponent
import io.jeffchang.template.core.DaggerCoreComponent
import io.jeffchang.template.di.DaggerAppComponent
import io.jeffchang.template.util.FlipperInitializer
import timber.log.Timber

class EmployeeApplication : Application() {

    private val coreComponent: CoreComponent by lazy {
        DaggerCoreComponent.builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        inject()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        initFlipper()
    }

    private fun initFlipper() {
        FlipperInitializer.init(this)
    }

    private fun inject() {
        DaggerAppComponent.builder()
            .coreComponent(coreComponent)
            .build()
            .inject(this)
    }

    companion object {
        @JvmStatic
        fun coreComponent(context: Context) =
            (context.applicationContext as EmployeeApplication).coreComponent
    }
}

fun Activity.coreComponent() = EmployeeApplication.coreComponent(this)

fun Fragment.coreComponent() = EmployeeApplication.coreComponent(requireContext())
