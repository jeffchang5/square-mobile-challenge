package io.jeffchang.template.domain.model

data class WeatherInfo(
    val temperature: Double,
    val humidity: Int,
    val pressure: Double
)
