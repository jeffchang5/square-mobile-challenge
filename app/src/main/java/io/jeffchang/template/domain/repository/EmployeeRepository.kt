package io.jeffchang.template.domain.repository

import io.jeffchang.template.core.data.Result
import io.jeffchang.template.data.model.Employee

interface EmployeeRepository {

    suspend fun getEmployees(): Result<List<Employee>>
}
