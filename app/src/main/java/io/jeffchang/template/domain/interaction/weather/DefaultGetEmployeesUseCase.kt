package io.jeffchang.template.domain.interaction.weather

import io.jeffchang.template.core.data.Result
import io.jeffchang.template.data.model.Employee
import io.jeffchang.template.domain.repository.EmployeeRepository

class DefaultGetEmployeesUseCase(
    private val employeeRepository: EmployeeRepository
) : GetEmployeesUseCase {

    override suspend fun invoke(param: Unit): Result<List<Employee>> {
        return employeeRepository.getEmployees()
    }
}
