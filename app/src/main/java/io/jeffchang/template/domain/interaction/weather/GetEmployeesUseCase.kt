package io.jeffchang.template.domain.interaction.weather

import io.jeffchang.template.core.data.Result
import io.jeffchang.template.data.model.Employee
import io.jeffchang.template.domain.base.BaseUseCase

interface GetEmployeesUseCase : BaseUseCase<Unit, List<Employee>> {

    override suspend operator fun invoke(param: Unit): Result<List<Employee>>
}
