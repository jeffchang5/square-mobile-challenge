package io.jeffchang.template.domain.repository

import io.jeffchang.template.core.data.ContextProvider
import io.jeffchang.template.core.data.Result
import io.jeffchang.template.core.data.safeApiCall
import io.jeffchang.template.data.model.Employee
import io.jeffchang.template.data.networking.service.EmployeeService
import kotlinx.coroutines.withContext

class DefaultEmployeeRepository(
    private val provider: ContextProvider,
    private val employeeService: EmployeeService
) : EmployeeRepository {

    override suspend fun getEmployees(): Result<List<Employee>> {
        return withContext(provider.io) {
            safeApiCall(employeeService::getEmployees)
        }
    }
}
