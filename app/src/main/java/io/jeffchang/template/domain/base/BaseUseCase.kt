package io.jeffchang.template.domain.base

import io.jeffchang.template.core.data.Result

interface BaseUseCase<T : Any, R : Any> {

    suspend operator fun invoke(param: T): Result<R>
}
