plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(Versions.Project.compileSdk)
    defaultConfig {
        minSdkVersion(Versions.Project.minSdk)
        targetSdkVersion(Versions.Project.targetSdk)

        buildConfigField("String", "BASE_URL", Constants.BASE_URL)

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        versionCode = 1
        versionName = "1.0"
    }
    viewBinding {
        isEnabled = true
    }
    buildTypes {
        named("debug") {}
        named("release") {
            isMinifyEnabled = false
            proguardFiles("proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

initSharedDependencies()
initTestDependencies()

dependencies {
    implementation(fileTree("src") {
        include(".jar")
    })
    implementation(Deps.Facebook.flipper)
    implementation(Deps.Square.moshiKotlin)
    implementation(Deps.Square.retrofitMoshiConverter)

    implementation(Deps.AndroidX.ViewModelKtx)

    implementation(Deps.Facebook.networkFlipperPlugin)
    implementation(Deps.Facebook.soLoader)
}
