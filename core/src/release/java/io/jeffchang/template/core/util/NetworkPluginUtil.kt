package io.jeffchang.template.core.util

import okhttp3.OkHttpClient

fun OkHttpClient.Builder.addFlipperPluginIfPresent() = apply {
    // no-opt
}
