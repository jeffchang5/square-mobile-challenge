package io.jeffchang.template.core.util

import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import io.jeffchang.template.core.BuildConfig
import okhttp3.OkHttpClient

fun OkHttpClient.Builder.addFlipperPluginIfPresent() = apply {
    if (BuildConfig.DEBUG) {
        val plugin = NetworkFlipperPlugin()
        addInterceptor(
            FlipperOkhttpInterceptor(plugin)
        )
    }
}
