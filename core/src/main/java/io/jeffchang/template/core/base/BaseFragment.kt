package io.jeffchang.template.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import io.jeffchang.template.core.R
import io.jeffchang.template.core.data.model.NetworkState
import io.jeffchang.template.core.data.model.ViewState
import io.jeffchang.template.core.databinding.FragmentBaseBinding
import io.jeffchang.template.core.ktx.invoke

enum class State {
    LOADING, NO_INTERNET, MAIN, EMPTY, ERROR
}

abstract class BaseFragment<T : Any> : Fragment() {

    abstract val viewModel: BaseViewModel<T>

    abstract var containerView: View

    private lateinit var binding: FragmentBaseBinding

    protected var state: State = State.LOADING
        set(value) {
            field = value
            binding.apply {
                when (value) {
                    State.LOADING -> {
                        progressView.isVisible = true
                        fragmentContainer.isVisible = false
                        noInternetView.isVisible = false
                        emptyListView.isVisible = false
                        errorView.isVisible = false
                    }
                    State.NO_INTERNET -> {
                        progressView.isVisible = false
                        fragmentContainer.isVisible = false
                        noInternetView.isVisible = true
                        emptyListView.isVisible = false
                        errorView.isVisible = false
                    }
                    State.MAIN -> {
                        progressView.isVisible = false
                        fragmentContainer.isVisible = true
                        noInternetView.isVisible = false
                        emptyListView.isVisible = false
                        errorView.isVisible = false
                    }
                    State.EMPTY -> {
                        progressView.isVisible = false
                        fragmentContainer.isVisible = false
                        noInternetView.isVisible = false
                        emptyListView.isVisible = true
                        errorView.isVisible = false
                    }
                    State.ERROR -> {
                        progressView.isVisible = false
                        fragmentContainer.isVisible = false
                        noInternetView.isVisible = false
                        emptyListView.isVisible = false
                        errorView.isVisible = true
                    }
                }
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBaseBinding.inflate(inflater)
            .also {
                it.fragmentContainer.addView(
                    containerView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeNetworkState()
    }

    private fun observeNetworkState() {
        binding.apply {
            viewModel.viewState()(viewLifecycleOwner) { viewState ->
                when (viewState) {
                    is ViewState.Loading -> {
                        state = State.LOADING
                    }
                    is ViewState.Error -> {
                        state = State.MAIN
                        Toast.makeText(context, viewState.error.message, Toast.LENGTH_LONG).show()
                    }
                    is ViewState.Success -> {
                        state = State.MAIN
                    }
                }
            }
            viewModel.networkState()(viewLifecycleOwner) { t ->
                when (t) {
                    is NetworkState.Disconnected -> {
                        Toast.makeText(context, R.string.disconnected, Toast.LENGTH_SHORT).show()
                    }
                    is NetworkState.Connected -> {
                        Toast.makeText(context, R.string.connected, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}
