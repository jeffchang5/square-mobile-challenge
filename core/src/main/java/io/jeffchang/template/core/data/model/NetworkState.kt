package io.jeffchang.template.core.data.model

sealed class NetworkState {
    object Connected : NetworkState()
    object Disconnected : NetworkState()
}
