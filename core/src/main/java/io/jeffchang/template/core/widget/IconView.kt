@file:Suppress("unused")

package io.jeffchang.template.core.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import io.jeffchang.template.core.databinding.ViewPlaceholderBinding

/**
 * View that displays a screen when the user has lost connectivity.
 */
abstract class IconView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: ViewPlaceholderBinding =
        ViewPlaceholderBinding.inflate(LayoutInflater.from(context), this, true)

    abstract var iconDrawable: Int

    abstract var titleText: Int

    override fun onFinishInflate() {
        super.onFinishInflate()
        binding.apply {
            iconImageView.setImageResource(iconDrawable)
            labelTextView.setText(titleText)
        }
    }
}
