package io.jeffchang.template.core.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.jeffchang.template.core.data.ContextProvider
import io.jeffchang.template.core.data.model.NetworkState
import io.jeffchang.template.core.data.model.ViewState
import io.jeffchang.template.core.network.Connectivity
import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class BaseViewModel<T : Any> constructor(
    private val contextProvider: ContextProvider,
    private val connectivity: Connectivity
) : ViewModel() {

    protected val viewState = MutableLiveData<ViewState<T>>()

    protected val networkState = MutableLiveData<NetworkState>()

    open var onConnectivityRegained = {}

    fun viewState(): LiveData<ViewState<T>> = viewState

    fun networkState(): LiveData<NetworkState> = networkState

    init {
        launch {
            connectivity.networkState.collect {
                if (it is NetworkState.Connected) {
                    onConnectivityRegained()
                }
                networkState.value = it
            }
        }
    }

    protected fun execute(action: suspend () -> Unit, noInternetAction: () -> Unit) {
        if (networkState.value is NetworkState.Disconnected) {
            launch { noInternetAction() }
        } else {
            viewState.value = ViewState.Loading()
            launch { action() }
        }
    }

    protected fun execute(
        action: suspend () -> Unit
    ) {
        viewState.value = ViewState.Loading()
        launch { action() }
    }

    private fun launch(
        coroutineContext: CoroutineContext = contextProvider.main,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return viewModelScope.launch(coroutineContext) { block() }
    }
}
