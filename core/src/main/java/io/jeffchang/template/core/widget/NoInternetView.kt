@file:Suppress("unused")

package io.jeffchang.template.core.widget

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import io.jeffchang.template.core.R

/**
 * View that displays a screen when the user has lost connectivity.
 */
class NoInternetView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : IconView(context, attrs, defStyleAttr) {

    @DrawableRes
    override var iconDrawable: Int = R.drawable.ic_cloud_off_black_24dp

    @StringRes
    override var titleText: Int = R.string.device_offline
}
