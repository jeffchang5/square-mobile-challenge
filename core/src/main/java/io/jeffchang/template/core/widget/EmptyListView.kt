@file:Suppress("unused")

package io.jeffchang.template.core.widget

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import io.jeffchang.template.core.R

/**
 * View that displays a screen when the results from an API call is empty.
 */
class EmptyListView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : IconView(context, attrs, defStyleAttr) {

    @DrawableRes
    override var iconDrawable: Int = R.drawable.ic_signal_cellular_0_bar_black_24dp

    @StringRes
    override var titleText: Int = R.string.device_offline
}
