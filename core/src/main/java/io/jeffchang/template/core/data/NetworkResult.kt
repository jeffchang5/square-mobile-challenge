package io.jeffchang.template.core.data

import com.squareup.moshi.JsonDataException
import java.io.IOException
import java.net.UnknownHostException
import retrofit2.HttpException
import retrofit2.Response

interface DomainMapper<T : Any> {
    fun mapToDomainModel(): T
}

interface RoomMapper<out T : Any> {
    fun mapToRoomEntity(): T
}

inline fun <T : Any> Response<T>.onSuccess(action: (T) -> Unit): Response<T> {
    if (isSuccessful) body()?.run(action)
    return this
}

inline fun <T : Any> Response<T>.onFailure(action: (NetworkException) -> Unit) {
    if (!isSuccessful) errorBody()?.run {
        action(KnownNetworkException(code(), message()))
    }
}

/**
 * Use this if you need to cache data after fetching it from the api, or retrieve something from cache
 */

inline fun <T : RoomMapper<R>, R : DomainMapper<U>, U : Any> Response<T>.getData(
    cacheAction: (R) -> Unit,
    fetchFromCacheAction: () -> R
): Result<U> {
    try {
        onSuccess {
            val databaseEntity = it.mapToRoomEntity()
            cacheAction(databaseEntity)
            return Success(databaseEntity.mapToDomainModel())
        }
        onFailure {
            val cachedModel = fetchFromCacheAction()
            Success(cachedModel.mapToDomainModel())
        }
        return Failure(UnknownNetworkException)
    } catch (e: IOException) {
        return Failure(UnknownNetworkException)
    }
}

/**
 * Use this when communicating only with the api service
 */
fun <T : DomainMapper<R>, R : Any> Response<T>.getData(): Result<R> {
    return try {
        onSuccess { return Success(it.mapToDomainModel()) }
        onFailure { return Failure(it) }
        Failure(UnknownNetworkException)
    } catch (e: IOException) {
        Failure(UnknownNetworkException)
    }
}

suspend fun <T : DomainMapper<R>, R : Any> safeApiCall(block: suspend () -> T): Result<R> {
    return try {
        Success(block().mapToDomainModel())
    } catch (e: Exception) {
        Failure(catchException(e))
    }
}

suspend fun <T : DomainMapper<R>, R : Any> safeApiCallFromList(block: suspend () -> List<T>): Result<List<R>> {
    return try {
        val domainModelList = block().map {
            it.mapToDomainModel()
        }
        Success(domainModelList)
    } catch (e: Exception) {
        Failure(catchException(e))
    }
}

private fun catchException(e: Exception): NetworkException {
    return when (e) {
        is HttpException -> KnownNetworkException(e.code(), e.response()?.toString().orEmpty())
        is UnknownHostException -> HostNotFoundNetworkException
        is JsonDataException -> BadResponseException
        else -> UnknownNetworkException
    }
}
