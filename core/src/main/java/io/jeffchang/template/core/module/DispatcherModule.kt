package io.jeffchang.template.core.module

import dagger.Module
import dagger.Provides
import io.jeffchang.template.core.data.ContextProvider
import io.jeffchang.template.core.data.DefaultContextProvider
import javax.inject.Singleton

@Module
class DispatcherModule {

    @Provides
    @Singleton
    fun provideDispatcher(): ContextProvider {
        return DefaultContextProvider()
    }
}
