package io.jeffchang.template.core.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import io.jeffchang.template.core.data.model.NetworkState
import javax.inject.Inject
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.debounce
import timber.log.Timber

interface Connectivity {

    val networkState: Flow<NetworkState>
}

private const val DEFAULT_CONNECT_DEBOUNCE_DURATION_IN_MILLIS = 200L

@Suppress("EXPERIMENTAL_API_USAGE")
class AndroidConnectivity @Inject constructor(
    val context: Context
) : Connectivity {

    private val connectivityManager by lazy {
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    override val networkState: Flow<NetworkState> = callbackFlow<NetworkState> {
        val callback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                super.onAvailable(network)
                Timber.d("Connected")
                offer(NetworkState.Connected)
            }

            override fun onLost(network: Network?) {
                super.onLost(network)
                Timber.d("No internet")
                offer(NetworkState.Disconnected)
            }
        }
            .also {
                connectivityManager.registerNetworkCallback(
                    NetworkRequest.Builder().build(),
                    it
                )
            }
        awaitClose {
            connectivityManager.unregisterNetworkCallback(callback)
        }
    }
        .debounce(DEFAULT_CONNECT_DEBOUNCE_DURATION_IN_MILLIS)
}
