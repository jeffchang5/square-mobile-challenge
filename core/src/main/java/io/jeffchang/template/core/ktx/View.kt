package io.jeffchang.template.core.ktx

import android.view.View

fun View.onClick(cb: () -> Unit) {
    setOnClickListener { cb() }
}
