package io.jeffchang.template.core.data

sealed class Result<out T : Any>
data class Success<out T : Any>(val data: T) : Result<T>()
data class Failure(val networkException: NetworkException) : Result<Nothing>()

sealed class NetworkException(
    open val errorCode: Int = 0,
    override val message: String = ""
) : Throwable(message)

data class KnownNetworkException(
    override val errorCode: Int,
    val errorBody: String
) : NetworkException(message = "Contains known error")

object UnknownNetworkException : NetworkException(message = "Unknown HTTP exception")

object HostNotFoundNetworkException : NetworkException(message = "Can't connect to the host")

object BadResponseException : NetworkException(message = "Got a bad response from the server")

inline fun <T : Any> Result<T>.onSuccess(action: (T) -> Unit): Result<T> {
    if (this is Success) action(data)
    return this
}

inline fun <T : Any> Result<T>.onFailure(action: (NetworkException) -> Unit) {
    if (this is Failure) action(networkException)
}
